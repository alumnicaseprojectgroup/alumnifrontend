import { BrowserRouter, Route, Routes } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css";
import Home from "./Pages/Home";
import NotFound from "./Pages/NotFound";
import TimeLine from "./Pages/TimeLine";
import UserProfile from "./Pages/User/UserProfile";
import Header from "./Components/Navbar/Header";
import { Dispatch, useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { api } from "./Utils/api";
import Topics from "./Pages/Topic/Topics";
import GroupPage from "./Pages/Group/GroupPage";
import SingleTopic from "./Pages/Topic/SingleTopic";
import ViewThread from "./Pages/Posts/ViewThread";
import { useDispatch } from "react-redux";
import { fetchUser } from "./store/actions/UserActions";
import DirectConversations from "./Pages/DirectMessages/DirectConversations";

function App() {
  // Hooks
  const { getAccessTokenSilently, isLoading } = useAuth0();
  const dispatch: Dispatch<any> = useDispatch();

  // State
  const [loading, setLoading] = useState(true);

  /**
   * Checks if user's api token isn't empty.
   * If true, makes api call to fetch user values from db.
   */
  useEffect(() => {
    if (isLoading) return;
    if(api.token === ""){
      getAccessTokenSilently().then(token => {
        api.token = token;
        dispatch(fetchUser());
        setLoading(false);
      })
        .catch(() => {
          api.token = "";
          setLoading(false);
        })
    }
    
    
  }, [getAccessTokenSilently, isLoading])

  return (
    <div className="App">
      {(loading) ? <p>Loading</p> :
        <>
          <BrowserRouter>
          <Header />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/TimeLine" element={<TimeLine />} />
              <Route path="/TimeLine/Group/:groupId" element={<TimeLine />} />
              <Route path="/Groups" element={<GroupPage />} />
              <Route path="/Groups/:userId" element={<GroupPage />} />
              <Route path="/UserProfile" element={<UserProfile />} />
              <Route path="/UserProfile/:userId" element={<UserProfile />} />
              <Route path="*" element={<NotFound />} />
              <Route path="/topics" element={<Topics/>} />           
              <Route path="/Thread/:postId" element={<ViewThread/>} />
              <Route path="/topics/:topicId" element={<SingleTopic/>} />
              <Route path="/DirectMessages" element={<DirectConversations/>} />
            </Routes>
          </BrowserRouter>
        </>
      }
    </div>
  );
}

export default App;

