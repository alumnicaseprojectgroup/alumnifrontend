import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Provider } from "react-redux";
import store from './store';
import { Auth0Provider } from "@auth0/auth0-react";
import App from "./App";

const uri = 'https://alumniwebapp.azurewebsites.net/TimeLine';
//const uri = 'http://localhost:3000/TimeLine';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
    <Auth0Provider
    domain="dev-0x2r-xoa.us.auth0.com"
    clientId="IM11aBvISPBqnYwPeMgVNxLzzOxPNe79"
    audience="https://AlumniAPI.com"
    scope="read:messages"
    redirectUri={uri}>
    <App />
  </Auth0Provider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
