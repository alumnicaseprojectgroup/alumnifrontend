export interface Posts {
    postId: string;
    timeStamp: string;
    parentPostId: string | null;
    senderId: string;
    targetId: string | null;
    postBody: string;
    targetGroupId: string;
    topicId: string;
    posterName: string;
    topicName: string;
    groupName: string;
}

export interface PostsCreate{
    parentPostId: string | null;
    targetId: string | null;
    postBody: string;
    targetGroupId: string | null;
    topicId: string | null;
}

export interface PostsEdit{
    postId: number;
    parentPostId: number | null;
    targetId: string | null;
    postBody: string;
    targetGroupId: number;
    topicId: number;
}

export interface PostsCreateAwnser{
    //postId: number;
    parentPostId: number;
    targetId: string;
    postBody: string;
    targetGroupId: string;
    topicId: string;
}