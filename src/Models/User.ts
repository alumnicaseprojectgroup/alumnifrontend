export interface User {
    userId: string;
    username: string;
    firstName: string;
    lastName: string;
    picture: string;
    emailAdress: string;
    status: string;
    bio: string;
    funFact: string;
    groups: number[];
    topics: number[];
}

export interface UserEdit {
    username: string;
    firstName: string;
    lastName: string;
    // picture: string;
    emailAdress: string;
    status: string;
    bio: string;
    funFact: string;
}

export interface conversationUser {
    senderId: string;
    posterName: string;
    targetId: string;
    targetName: string;
}
