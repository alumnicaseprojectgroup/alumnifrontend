export interface Group {
    groupId: number;
    name: string;
    description: string;
    isPrivate: boolean;
}

export interface GroupUser {
    userId: string | null;
}