
export interface Topic {
    topicId: number,
    name: string,
    description: string
}

export interface TopicUser {
    userId: number | null,
}