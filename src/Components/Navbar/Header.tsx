import { useAuth0 } from "@auth0/auth0-react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";

export default function Header() {
  // Auth0 Hook to check if user is authenticated and logout method.
  const { isAuthenticated, logout } = useAuth0();

  return (
    <>
      {!isAuthenticated ? (
        <Navbar
          bg="light"
          variant="light"
          sticky="top"
          style={{ boxShadow: "0px 0px 3px gray" }}
        >
          <Container>
            <Navbar.Brand href={"/"}>AlumniNetwork</Navbar.Brand>
          </Container>
        </Navbar>
      ) : (
        <Navbar
          bg="light"
          variant="light"
          sticky="top"
          style={{ boxShadow: "0px 0px 3px gray" }}
        >
          <Container>
            <Navbar.Brand href={"/"}>AlumniNetwork</Navbar.Brand>
            <Nav>
              <NavLink
                to="/"
                style={{
                  marginLeft: "10px",
                  textDecoration: "none",
                  color: "gray",
                }}
              >
                Home
              </NavLink>
              <NavLink
                to="/DirectMessages"
                style={{
                  marginLeft: "10px",
                  textDecoration: "none",
                  color: "gray",
                }}
              >
                Direct Messages
              </NavLink>
              <NavLink
                to="/Groups"
                style={{
                  marginLeft: "10px",
                  textDecoration: "none",
                  color: "gray",
                }}
              >
                Groups
              </NavLink>
              <NavLink
                to="/Topics"
                style={{
                  marginLeft: "10px",
                  textDecoration: "none",
                  color: "gray",
                }}
              >
                Topics
              </NavLink>
              <NavLink
                to="/UserProfile"
                style={{
                  marginLeft: "10px",
                  textDecoration: "none",
                  color: "gray",
                }}
              >
                Profile
              </NavLink>
              <NavLink to="" onClick={() => logout()} style={{
                  marginLeft: "10px",
                  color: "gray",
                }}>Logout</NavLink>
            </Nav>
          </Container>
        </Navbar>
      )}
    </>
  );
}