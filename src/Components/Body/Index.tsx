import { Container } from "react-bootstrap";
import "./body.css";

/**
 * Global body used in all pages
 * @param props any html code in body tag
 */

export default function Index(props: any) {
  return <Container className="body-container">{props.children}</Container>;
}
