import { useEffect, useState } from "react";
import { conversationUser } from "../../Models/User";
import { api } from "../../Utils/api";
import Conversation from "./Conversation";
import Body from "../../Components/Body/Index";
import { Card, Tab, Tabs } from "react-bootstrap";
import { useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import { useAuth0 } from "@auth0/auth0-react";
import { Navigate } from "react-router-dom";



export default function DirectConversations() {
    // Auth0 hook to check if user is authenticated
    const { isAuthenticated } = useAuth0();

    // State
    const [conversationPartners, setConversationPartners] = useState<conversationUser[]>([]);
    const { user: account, loading } = useSelector((state: RootState) => state.userData);

    /**
     * Gets all conversations between two users from db.
     */
    useEffect(() => {
        api.getDirectMessages()
            .then(data => setConversationPartners(data));
    }, [])

    return (
        <>
        {!isAuthenticated ?
        <Navigate to="/" />
        :
        (<Body>
            {loading ? 
            (<p>Loading ...</p>)
            :
            (conversationPartners.length > 0 ?
                <Tabs defaultActiveKey={`tabs-${conversationPartners[0].senderId}`}>
                    {conversationPartners.map((partner: conversationUser) => (
                        <Tab title={(partner.senderId === account.userId) ? partner.targetName : partner.posterName} key={`tabkey-${(partner.senderId === account.userId) ? partner.targetId : partner.senderId}`} eventKey={`tab-${(partner.senderId === account.userId) ? partner.targetId : partner.senderId}`}>
                            <Card key={`card-${(partner.senderId === account.userId) ? partner.targetName : partner.posterName}`}>
                                <h2 style={{textAlign: "center", borderBottom: "solid 1px black"}} key={`header-${(partner.senderId === account.userId) ? partner.targetName : partner.posterName}`}>{(partner.senderId === account.userId) ? partner.targetName : partner.posterName}</h2>

                                <Conversation key={`convo-${(partner.senderId === account.userId) ? partner.targetName : partner.posterName}`} senderId={(partner.senderId === account.userId) ? partner.targetId : partner.senderId} targetName={(partner.senderId === account.userId) ? partner.targetName : partner.posterName}/>
                            </Card>
                        </Tab>


                    ))}
                </Tabs>
                : <h2>No conversations</h2>)
        }
        </Body>)
        }
        </>
    );
}
