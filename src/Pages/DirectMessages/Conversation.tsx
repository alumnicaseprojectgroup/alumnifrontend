import { FC, useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { Posts } from "../../Models/Posts";
import { api } from "../../Utils/api";
import CreatePost from "../Modals/CreatePost";

// Props sent from DirectConversations Component.
interface ConvProp {
  senderId: string;
  targetName: string;
}

const Conversation: FC<ConvProp> = ({ senderId, targetName }) => {
  // State
  const [conversation, setConversation] = useState<Posts[]>([]);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");

  
  /**
   * Api call to get your conversations
   */
  useEffect(() => {
    getConvo();
  }, []);

  const getConvo = () => api.GetConversationById(senderId).then((data) => setConversation(data));

  

  /**
   * Functions to handle open and close of modal.
   * @returns Boolean if modal is open or not
   */
   const handleOpenModal = () => setOpen(true);
   const handleCloseModal = () => {
     setOpen(false);
     setTimeout(() => getConvo(), 500);
   }

  return (
    <>
      <Button
        key={`btn-${senderId}`}
        className="btn bg-secondary btn-outline-light"
        onClick={() => handleOpenModal()}
      >
        New Message
      </Button>
      {conversation.map((post: Posts) =>
        post.senderId === senderId ? (
          <p key={`target-${post.postId}`}>
            <b>{targetName}: </b>
            {post.postBody}
          </p>
        ) : (
          <p key={`user-${post.postId}`} style={{ textAlign: "right" }}>
            {post.postBody} <b>: You</b>
          </p>
        )
      )}

      <Modal
        title="Create Post"
        error={message}
        show={open}
        onHide={handleCloseModal}
      >
        <CreatePost
          title="New message"
          userId={senderId}
          handleCloseModal={handleCloseModal}
          fromProfilePage={false}
        ></CreatePost>
      </Modal>
    </>
  );
};

export default Conversation;
