import { Dispatch, useEffect, useState } from "react";
import Body from "../../Components/Body/Index";
import { Button, Card, Col, Container, Modal, Row } from "react-bootstrap";
import { api } from "../../Utils/api";
import { Group } from "../../Models/Group";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import { fetchUser } from "../../store/actions/UserActions";
import { useAuth0 } from "@auth0/auth0-react";
import CreateGroup from "../Modals/CreateGroup";

export default function GroupPage() {
  const { isAuthenticated } = useAuth0();
  const [groupList, setGroupList] = useState<Group[]>([]);
  const [userToInviteGroups, setUserToInviteGroups] = useState<number[]>([]);
  const navigate = useNavigate();
  const { userId } = useParams();
  const dispatch: Dispatch<any> = useDispatch();
  const { user: account, loading } = useSelector((state: RootState) => state.userData);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (isAuthenticated) {
      fetchGroups();
      if (userId !== undefined) {
        api
          .getUserByID(userId)
          .then((user) => setUserToInviteGroups(user.groups));
      }
    }
  }, []);

  const HandleCreateGroupClick = () => {
    navigate("/CreateGroup");
  };

  const fetchGroups = () => {
    api.getGroups().then((data) => {
      setGroupList(data);
      dispatch(fetchUser());
    });

  };

  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);

  return (
    <>
      {!isAuthenticated ? (
        <Navigate to="/" />
      ) : (
        <Body>
         {loading ?(<p>Loading ...</p>):(<>
          <h4 style={{ textAlign: "center" }}>Groups</h4><div style={{ textAlign: "right", marginBottom: "15px" }}>
                <Button
                  className="btn bg-secondary btn-outline-light"
                  onClick={handleOpenModal}
                >
                  Create new Group
                </Button>
              </div>
          {groupList.length > 0 ? (
            <Container>
              <Row className="g-4">
                {groupList.map((group) => {
                  return userId === undefined ? (
                    <Col sm={6} key={group.groupId}>
                      <Card
                        className="card"
                        key={group.groupId}
                        style={{ height: "350px" }}
                      >
                        <Card.Body className="card-body">
                          <div
                            onClick={() =>
                              navigate(`/TimeLine/Group/${group.groupId}`)
                            }
                          >
                            <label style={{ fontWeight: "bold" }}>
                              {group.name}
                            </label>
                            <br />
                            {group.description}
                            <br />
                            {group.isPrivate ? <p>private group</p> : ""}
                          </div>
                        </Card.Body>
                        {account.groups.includes(group.groupId) ? (
                          <Button variant="success" disabled>Joined</Button>
                        ) : (
                          <Button
                            onClick={() =>
                              api
                                .joinGroup(group.groupId)
                                .then(() => dispatch(fetchUser()))
                            }
                          >
                            Join
                          </Button>
                        )}
                      </Card>
                    </Col>
                  ) : account.groups.includes(group.groupId) ? (
                    <Col sm={6} key={group.groupId}>
                      <Card
                        className="card"
                        key={group.groupId}
                        style={{ height: "fit-content" }}
                      >
                        <Card.Body className="card-body">
                          <label
                            style={{ fontWeight: "bold" }}
                            onClick={() =>
                              navigate(`/TimeLine/Group/${group.groupId}`)
                            }
                          >
                            {group.name}
                          </label>
                          <br />
                          {group.description}
                          <br />
                          {group.isPrivate ? <p>private group</p> : ""}
                          {userToInviteGroups.includes(group.groupId) ? (
                            <p>already a member</p>
                          ) : (
                            <Button
                              onClick={() =>
                                api
                                  .addToGroup(group.groupId, { userId: userId })
                                  .then(() => navigate(-1))
                              }
                            >
                              Invite
                            </Button>
                          )}
                        </Card.Body>
                      </Card>
                    </Col>
                  ) : (
                    ""
                  );
                })}
              </Row>
            </Container>
          ) : (
            ""
          )}
        </>)} 
        </Body>
      )}
      <Modal
        title="Create Post"
        error={message}
        show={open}
        onHide={handleCloseModal}
      >
        <CreateGroup handleCloseModal={handleCloseModal} fetchGroups={fetchGroups} ></CreateGroup>
      </Modal>
    </>
  );
}
