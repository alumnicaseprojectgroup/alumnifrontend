import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Posts } from "../../Models/Posts";
import { api } from "../../Utils/api";
import Body from "../../Components/Body/Index";
import { Card } from "react-bootstrap";

export default function ViewThread() {
  // Hooks
  const { postId } = useParams();
  const navigate = useNavigate();

  // State
  const [thread, setThread] = useState<Posts[]>([]);

  /**
   * Makes api call to get all posts that have a relation with parent post.
   */
  useEffect(() => {
    if (postId !== undefined) {
      api.getThreadById(parseInt(postId)).then((data) => setThread(data));
    }
  }, []);

  return (
    <>
      <Body>
        {thread.length > 0 ? (
          <div>
            {thread.map((post) => (
              <Card
                className="card"
                key={post.postId}
                style={{ height: "fit-content" }}
              >
                <Card.Body className="card-body">
                  <label
                    style={{ fontWeight: "bold" }}
                    onClick={() => navigate(`/UserProfile/${post.senderId}`)}
                  >
                    {post.posterName}
                  </label>
                  {post.parentPostId === null ? (
                    <p>says:</p>
                  ) : (
                    <p>responded with:</p>
                  )}
                  {post.postBody}
                </Card.Body>
                {post.timeStamp.split("T").join(" ").split(".")[0]}
                <br />
              </Card>
            ))}
          </div>
        ) : (
          <h2></h2>
        )}
      </Body>
    </>
  );
}
