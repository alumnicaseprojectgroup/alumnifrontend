import { FC } from "react";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import { api } from "../../Utils/api";

/**
 * Props sent from parent component.
 */
interface TopicProps{
  handleCloseModal: ()=> void;
  fetchTopics: ()=> void;
}

const CreateTopic: FC<TopicProps> = ({handleCloseModal, fetchTopics}) => {

  /**
   * Handles values when creating a new topic.
   * @param e Gets values from inputs when creating a new topic.
   */
  function handleSubmit(e: any): void {
    e.preventDefault();

    const name: string = e.target.name.value;
    const description: string = e.target.description.value;

    const topic = {
      name,
      description,
    };
    api.postTopic(topic).then(() => fetchTopics());
  }


  return (
    <div className="modal-body">
        <Form onSubmit={handleSubmit} style={{width: "100%"}}>
          <h5 style={{ textAlign: "center" }}>Create a Topic</h5>
          <InputGroup className="mb-3">
            <FormControl
              aria-label="name"
              aria-describedby="inputGroup-sizing-default"
              name="name"
              id="name"
            ></FormControl>
            <InputGroup.Text id="inputGroup-sizing-default">
              Name
            </InputGroup.Text>
          </InputGroup>
          <InputGroup className="mb-3">
            <FormControl
              aria-label="description"
              aria-describedby="inputGroup-sizing-default"
              name="description"
              id="description"
            ></FormControl>
            <InputGroup.Text id="inputGroup-sizing-default">
              Description
            </InputGroup.Text>
          </InputGroup>
          <Button type="submit" id="modal-btn" className="btn primary" onClick={() => handleCloseModal()}>
            Create
          </Button>
          <Button
              type="button"
              id="modal-btn"
              className="btn btn-secondary"
              onClick={() => handleCloseModal()}
            >
              {" "}
              Go back{" "}
            </Button>
        </Form>
    </div>
  );
}

export default CreateTopic;