import { FC, useState } from "react";
import { Button, Form, FormControl, InputGroup } from "react-bootstrap";
import { api } from "../../Utils/api";

/**
 * Props sent from parent component.
 */
interface CreateGroupProps {
  handleCloseModal: () => void;
  fetchGroups: () => void;
}

const CreateGroup: FC<CreateGroupProps> = ({ handleCloseModal, fetchGroups }) => {
  // Check if group is going to be private or not.
  const [isChecked, setIsChecked] = useState(false);

  /**
   * Handles values when creating a new group.
   * Sends a call to api to get a list with the new group.
   * @param e Gets values from inputs when creating a new group.
   */
  function HandleSubmit(e: any): void {
    e.preventDefault();
    const name: string = e.target.GroupName.value;
    const description: string = e.target.description.value;
    const isPrivate: boolean = isChecked;
    const group = {
      name,
      description,
      isPrivate,
    };
    api.postGroup(group).then(() => fetchGroups());
  }
  // sets isChecked to opposite when the checkbox is checked/unchecked
  const handleChange = () => {
    setIsChecked(!isChecked);
  }

  return (
    <div className="modal-body">
      <Form onSubmit={HandleSubmit}>
        <h5 style={{ textAlign: "center" }}>Create a group</h5>
        <InputGroup>
          <FormControl
            aria-label="GroupName"
            aria-describedby="inputGroup-sizing-default"
            name="GroupName"
            id="GroupName"
          />
          <InputGroup.Text id="inputGroup-sizing-default">
            Group Name
          </InputGroup.Text>
        </InputGroup>
        <InputGroup>
          <FormControl
            aria-label="description"
            aria-describedby="inputGroup-sizing-default"
            name="description"
            id="description"
          />
          <InputGroup.Text id="inputGroup-sizing-default">
            Description
          </InputGroup.Text>
        </InputGroup>
        <label>
          <input
            type="checkbox"
            role="switch"
            id="isPrivate"
            onChange={() => handleChange()}
          />
          Private Group
        </label>
        <div>
          <Button
            type="submit"
            id="modal-btn"
            className="btn primary"
            onClick={() => handleCloseModal()}
          >
            Create
          </Button>
        </div>
        <Button
          type="button"
          id="modal-btn"
          className="btn btn-secondary"
          onClick={() => handleCloseModal()}
        >
          Go back
        </Button>
      </Form>
    </div>
  );
};

export default CreateGroup;
