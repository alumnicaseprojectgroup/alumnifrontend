import { useAuth0 } from "@auth0/auth0-react";
import { Dispatch, FC, useEffect, useState } from "react";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Group } from "../../Models/Group";
import { Topic } from "../../Models/Topic";
import { postPost } from "../../store/actions/TimeLineActions";
import { RootState } from "../../store/reducers";
import { api } from "../../Utils/api";

/**
 * Props sent from parent component.
 */
interface CreatePostProps {
  userId: string;
  title: string,
  handleCloseModal: () => void;
  fromProfilePage: boolean;
}

 const CreatePost: FC<CreatePostProps> = ({ userId, title, handleCloseModal, fromProfilePage }) => {
   // Hooks
  const dispatch: Dispatch<any> = useDispatch();
  const navigate = useNavigate();

  // State
  const { user: account } = useSelector((state: RootState) => state.userData);
  const [groups, setGroups] = useState<Group[]>([]);
  const [topics, setTopics] = useState<Topic[]>([]);

  /**
   * Checks if useris isEmpty. if false, gets all groups and topics that user has joined.
   */
  useEffect(() => {
    if (userId === "") {
      for (const groupId of account.groups) {
        api
          .getGroupById(groupId)
          .then((group) => setGroups((prevState) => [...prevState, group]));
      }
      for (const topicId of account.topics) {
        api
          .getTopicById(topicId)
          .then((topic) => setTopics((prevState) => [...prevState, topic]));
      }
    }
  }, []);

  /**
   * Handles values when creating a new post.
   * @param e Gets values from inputs when creating a new post.
   */
  function handleSubmit(e: any): void {
    e.preventDefault();

    let parentPostId: string | null = null; //Can input values or leave it null
    let targetId: string | null = null;
    const postBody: string = e.target.postBody.value;
    let targetGroupId: string | null = null;
    let topicId: string | null = null;
    
    if (userId !== "") {
      targetId = userId;
    } else {
      if(e.target.topicId.value !== ""){
        topicId = e.target.topicId.value;
      }
      if(e.target.targetGroupId.value !== ""){
        targetGroupId = e.target.targetGroupId.value;
      }
    }
    
    const CreatePost = {
      parentPostId,
      targetId,
      postBody,
      targetGroupId,
      topicId,
    };
    dispatch(postPost(CreatePost));
    
    if(fromProfilePage)
      api.getGroups().then(()=> navigate(`/DirectMessages`));
  }

  return (
    <div className="modal-body">
          <Form className="form" onSubmit={handleSubmit} >
            <h5 style={{ textAlign: "center" }}>{title}</h5>
            <InputGroup className="mb-3" style={{ height: "200px" }}>
              <FormControl
                as="textarea"
                name="postBody"
                id="postBody"
                style={{maxHeight: "auto", resize: "none"}}
              />
            </InputGroup>
            {userId === "" ? (
              <InputGroup className="mb-3">
                <Form.Select
                  aria-label="targetGroupId"
                  aria-describedby="inputGroup-sizing-default"
                  name="targetGroupId"
                  id="targetGroupId"
                >
                  <option value={""}>Choose group</option>
                  {groups.map((group) => (
                    <option value={group.groupId} key={group.groupId}>
                      {group.name}
                    </option>
                  ))}
                </Form.Select>
              </InputGroup>
            ) : (
              ""
            )}
            {userId === "" ? (
              <InputGroup className="mb-3">
                <Form.Select
                  aria-label="topicId"
                  aria-describedby="inputGroup-sizing-default"
                  name="topicId"
                  id="topicId"
                >
                  <option value={""}>Choose topic</option>
                  {topics.map((topic) => (
                    <option value={topic.topicId} key={topic.topicId}>
                      {topic.name}
                    </option>
                  ))}
                </Form.Select>
              </InputGroup>
            ) : (
              ""
            )}
            <Button
              type="button"
              id="modal-btn"
              className="btn btn-secondary"
              onClick={() => handleCloseModal()}
            >
              {" "}
              Go back{" "}
            </Button>

            <Button
              type="submit"
              id="modal-btn"
              className="btn primary"
              onClick={() => handleCloseModal()}
            >
              {" "}
              Create{" "}
            </Button>
          </Form>
    </div>
  );
}

export default CreatePost;
