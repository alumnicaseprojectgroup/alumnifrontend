import { Dispatch, FC } from "react";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { AwnserPost, fetchPosts } from "../../store/actions/TimeLineActions";
import { PostsCreateAwnser } from "../../Models/Posts";

/**
 * Props sent from parent component
 */
interface ReplyProps {
  parentPostId: number | undefined;
  handleCloseModal: () => void;
}

const Reply: FC<ReplyProps> = ({ parentPostId, handleCloseModal }) => {
  // Hooks
  const dispatch: Dispatch<any> = useDispatch();

  /**
   * Handles values when replying to a post.
   * Makes api call that joins reply with parent post.
   * @param e Gets values from inputs when replying to a post.
   */
  function handleSubmit(e: any): void {
    e.preventDefault();

    if (parentPostId !== undefined) {
      const targetId: string = e.value || null;
      const postBody: string = e.target.postBody.value;
      const targetGroupId: string = e.value || null;
      const topicId: string = e.value || null;

      const PostsAwnser: PostsCreateAwnser = {
        parentPostId: parentPostId,
        targetId,
        postBody,
        targetGroupId,
        topicId,
      };
      dispatch(AwnserPost(PostsAwnser, parentPostId!)); // ! to make sure the code knows it is NOT null
      dispatch(fetchPosts());
    }
  }

  return (
    <div className="modal-body">
      <Form className="form" onSubmit={handleSubmit}>
        <h5 style={{ textAlign: "center" }}>Reply to the post</h5>

        <InputGroup className="mb-3" style={{ height: "200px" }}>
          <FormControl as="textarea" name="postBody" id="postBody" style={{maxHeight: "auto", resize: "none"}}/>
        </InputGroup>

        <Button
          type="button"
          id="modal-btn"
          className="btn btn-secondary"
          onClick={() => handleCloseModal()}
        >
          {" "}
          Go back{" "}
        </Button>

        <Button
          type="submit"
          id="modal-btn"
          className="btn primary"
          onClick={() => handleCloseModal()}
        >
          {" "}
          Post responce{" "}
        </Button>
      </Form>
    </div>
  );
};

export default Reply;
