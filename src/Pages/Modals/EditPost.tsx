import { Dispatch, FC, useEffect, useState } from "react";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { patchPost } from "../../store/actions/TimeLineActions";
import { Posts, PostsEdit } from "../../Models/Posts";
import { api } from "../../Utils/api";

/**
 * Props sent from parent component
 */
interface EditPostProps {
  postId: number | undefined;
  handleCloseModal: () => void;

}

const EditPosts: FC<EditPostProps> = ({ postId, handleCloseModal }) => {
  // Hooks
  const dispatch: Dispatch<any> = useDispatch();

  // State
  const [post, setPost] = useState<Posts>({
    postId: "",
    timeStamp: "",
    parentPostId: "",
    senderId: "",
    targetId: "",
    postBody: "",
    targetGroupId: "",
    topicId: "",
    posterName: "",
    topicName: "",
    groupName: ""
  });
  /** 
   * Gets the post that the user wants to edit by Id. 
  */
  useEffect(() => {
    if (postId !== undefined) {
      api.GetPostById(postId).then((data) => setPost(data));
    }
  }, []);

  /**
   * Handles values when editing a post.
   * Makes api call that patches post in database with the new values.
   * @param e Gets values from inputs when editing a existing post.
   */
  function handleSubmit(e: any): void {
    e.preventDefault();

    const postBody: string = e.target.postBody.value;

    const EditPost: PostsEdit = {
      postId: parseInt(post.postId),
      parentPostId: parseInt(
        post.parentPostId !== null ? post.parentPostId : ""
      ),
      targetId: post.targetId,
      postBody,
      targetGroupId: parseInt(post.targetGroupId),
      topicId: parseInt(post.topicId)
    };
    dispatch(patchPost(EditPost, parseInt(post.postId)));
  }

  return (
    <div className="modal-body">
      <Form className="form" onSubmit={handleSubmit}>
        <h5 style={{ textAlign: "center" }}>Edit Post</h5>

        <InputGroup className="mb-3" style={{ height: "200px" }}>
          <FormControl
            as="textarea"
            name="postBody"
            id="postBody"
            defaultValue={post.postBody}
            style={{maxHeight: "auto", resize: "none"}}
          />
        </InputGroup>

        <Button
          type="button"
          id="modal-btn"
          className="btn btn-secondary"
          onClick={() => handleCloseModal()}
        >
          {" "}
          Go back{" "}
        </Button>

        <Button
          type="submit"
          id="modal-btn"
          className="btn primary"
          onClick={() => handleCloseModal()}
        >
          {" "}
          Update{" "}
        </Button>
      </Form>
    </div>
  );
};

export default EditPosts;
