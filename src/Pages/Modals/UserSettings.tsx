import { Dispatch, FC, useEffect } from "react";
import { Button, Form, FormControl, InputGroup } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser, patchUser } from "../../store/actions/UserActions";
import { RootState } from "../../store/reducers";

/**
 * Props sent from parent component
 */
interface Props{
  handleCloseModal: () => void;
}

const UserSettings: FC<Props> = ({handleCloseModal}) =>  {
  // Hooks
  const dispatch: Dispatch<any> = useDispatch();

  // State
  const { user: account } = useSelector((state: RootState) => state.userData);

  /** 
   * Makes api call to fetch current user.
   */
  useEffect(() => {
    dispatch(fetchUser());
  }, []);

  /**
   * Handles values when editing user settings.
   * Makes api call to patch user with new values.
   * @param e Gets values from inputs when editing user settings. 
   */
  function handleSubmit(e: any): void {
    e.preventDefault();

    const username: string = e.target.username.value;
    const firstName: string = e.target.firstName.value;
    const lastName: string = e.target.lastName.value;
    const emailAdress: string = e.target.emailAdress.value;
    const status: string = e.target.status.value;
    const bio: string = e.target.bio.value;
    const funFact: string = e.target.funfact.value;

    const updatedUser = {
      username,
      firstName,
      lastName,
      emailAdress,
      status,
      bio,
      funFact,
    };
    dispatch(patchUser(updatedUser, account.userId));
    dispatch(fetchUser());
    handleCloseModal();
  }

  return (
    <div className="modal-body">
        <Form className="form" onSubmit={handleSubmit}>
          <h5 style={{ textAlign: "center" }}>Update Profile</h5>
          <InputGroup className="mb-3">
            <FormControl
              aria-label="username"
              aria-describedby="inputGroup-sizing-default"
              name="username"
              id="username"
              defaultValue={account.username}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Username
            </InputGroup.Text>
          </InputGroup>

          <InputGroup className="mb-3">
            <FormControl
              aria-label="firstName"
              aria-describedby="inputGroup-sizing-default"
              name="firstName"
              id="firstName"
              defaultValue={account.firstName}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Firstname
            </InputGroup.Text>
          </InputGroup>

          <InputGroup className="mb-3">
            <FormControl
              aria-label="lastName"
              aria-describedby="inputGroup-sizing-default"
              name="lastName"
              id="lastName"
              defaultValue={account.lastName}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Lastname
            </InputGroup.Text>
          </InputGroup>

          <InputGroup className="mb-3">
            <FormControl
              aria-label="emailAdress"
              aria-describedby="inputGroup-sizing-default"
              name="emailAdress"
              id="emailAdress"
              defaultValue={account.emailAdress}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Email
            </InputGroup.Text>
          </InputGroup>

          <InputGroup className="mb-3">
            <FormControl
              aria-label="status"
              aria-describedby="inputGroup-sizing-default"
              name="status"
              id="status"
              defaultValue={account.status}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Status
            </InputGroup.Text>
          </InputGroup>

          <InputGroup className="mb-3">
            <Form.Control
              as="textarea"
              name="bio"
              id="bio"
              defaultValue={account.bio}
              style={{ resize: "none" }}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Bio
            </InputGroup.Text>
          </InputGroup>

          <InputGroup className="mb-3">
            <Form.Control
              as="textarea"
              name="funfact"
              id="funfact"
              defaultValue={account.funFact}
              style={{ height: "100px", resize: "none" }}
            />
            <InputGroup.Text id="inputGroup-sizing-default">
              Fun Fact
            </InputGroup.Text>
          </InputGroup>

          <Button
            type="button"
            id="btn"
            className="btn btn-secondary"
            onClick={handleCloseModal}
            style={{float: "right", marginLeft: "10px"}}
          >
            {" "}
            Go back{" "}
          </Button>

          <Button type="submit" id="btn" className="btn primary" style={{float: "right", marginLeft: "10px"}}>
            {" "}
            Update{" "}
          </Button>
        </Form>
    </div>
  );
}

export default UserSettings;