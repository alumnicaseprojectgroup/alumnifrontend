import LoginButton from "./LoginButton";
import Body from "../Components/Body/Index";
import { useAuth0 } from "@auth0/auth0-react";
import { Navigate } from "react-router-dom";

export default function Home() {
  const { isAuthenticated } = useAuth0();

  return (
    <>
      {isAuthenticated ? (
        <Navigate to="/timeline" />
      ) : (
        <Body>
          <h2>Welcome to the Alumni Network!</h2>
          <h2>Press the Login button to sign in</h2>
          <LoginButton />
        </Body>
      )}
    </>
  );
}
