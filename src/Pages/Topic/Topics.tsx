import { Dispatch, useEffect, useState } from "react";
import { Button, Card, Col, Container, Modal, Row } from "react-bootstrap";
import { Topic } from "../../Models/Topic";
import { api } from "../../Utils/api";
import Body from "../../Components/Body/Index";
import { Navigate, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import { useAuth0 } from "@auth0/auth0-react";
import { fetchUser } from "../../store/actions/UserActions";
import CreateTopic from "../Modals/CreateTopic";

export default function Topics() {
  // Hooks
  const { isAuthenticated } = useAuth0();
  const navigate = useNavigate();
  const dispatch: Dispatch<any> = useDispatch();

  // State
  const [topics, setTopics] = useState<Topic[]>([]);
  const { user: account, loading } = useSelector((state: RootState) => state.userData);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");

  /**
   * Checks if user is authenticated. If true, calls function to fetch all topics.
   */
  useEffect(() => {
    if (isAuthenticated) {
      fetchTopics();
    }
  }, []);

  /**
   * Makes api call to fetch all topics from db.
   * @returns List of all topics.
   */
  const fetchTopics = () => api.getTopics().then((data) => {
    setTopics(data);
    dispatch(fetchUser());
  });

  /**
   * Functions to handle open and close of modal.
   * @returns Boolean if modal is open or not
   */
  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);

  return (
    <>
      {!isAuthenticated ? (
        <Navigate to="/" />
      ) : (
        <Body>
          {loading ? (<p>Loading ...</p>):(<>
          <h4 style={{ textAlign: "center" }}>Topics</h4>
          <div style={{ textAlign: "right", marginBottom: "15px" }}>
            <Button className="btn bg-secondary btn-outline-light" onClick={handleOpenModal}>New topic</Button>
          </div>
          <Container >
            <Row className="g-4">
              {topics.map((topic) => (
                <Col sm={6} key={topic.topicId}>
                  <Card
                    className="topic-card"
                    style={{ height: "350px" }}
                  >
                    <Card.Body onClick={() => navigate(`/Topics/${topic.topicId}`)}>
                      <label style={{ fontWeight: "bold" }}>{topic.name}</label>
                      <br />
                      {topic.description}
                    </Card.Body>
                    {account.topics.includes(topic?.topicId) ? (
                      <Button variant="success" disabled>Joined</Button>
                    ) : (
                      <Button
                        onClick={() =>
                          api
                            .joinTopic(topic.topicId)
                            .then(() => dispatch(fetchUser()))
                        }
                      >
                        Join
                      </Button>
                    )}
                  </Card>
                </Col>
              ))}
            </Row>
          </Container>
          </>)}
        </Body>
      )}
      <Modal
        title="Create Topic"
        error={message}
        show={open}
        onHide={handleCloseModal}
      >
        <CreateTopic handleCloseModal={handleCloseModal} fetchTopics={fetchTopics}></CreateTopic>
      </Modal>
    </>
  );
}
