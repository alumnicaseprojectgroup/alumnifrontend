import { useEffect, useState } from "react";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { Topic } from "../../Models/Topic";
import { api } from "../../Utils/api";
import Body from "../../Components/Body/Index";
import { Button, Modal } from "react-bootstrap";
import { useAuth0 } from "@auth0/auth0-react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import CreatePost from "../Modals/CreatePost";

export default function SingleTopic() {
  // Hooks
  const { isAuthenticated } = useAuth0();
  const { topicId } = useParams();
  const navigate = useNavigate();

  // State
  const { user: account } = useSelector((state: RootState) => state.userData);
  const [topic, setTopic] = useState<Topic>();
  const [message, setMessage] = useState("");
  const [open, setOpen] = useState(false);

  /**
   * Checks if topicId isn't undefined. If true gets topic by topicId.
   */
  useEffect(() => {
    if (topicId !== undefined) {
      api.getTopicById(parseInt(topicId)).then((data) => setTopic(data));
    }
  }, []);

    /**
   * Functions to handle open and close of modal.
   * @returns Boolean if modal is open or not
   */
  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);

  return (
    <>
      {!isAuthenticated ? (
        <Navigate to="/" />
      ) : (
        <Body>
          <div className="header">
            <a className="" onClick={handleOpenModal}>
              Create Post
            </a>
          </div>
          <h5>{topic?.name}</h5>
          <p>{topic?.description}</p>
          <div style={{float: "right"}}>
            <Button onClick={() => navigate(-1)}>Go back</Button>
          </div>
        </Body>
      )}
      <Modal
        title="Create Topic"
        error={message}
        show={open}
        onHide={handleCloseModal}
      >
        <CreatePost title="Create Topic" userId={""} handleCloseModal={handleCloseModal} fromProfilePage={false}></CreatePost>
      </Modal>
    </>
  );
}
