import Body from "../Components/Body/Index";
import { Button, Card, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store/reducers";
import { Dispatch, useEffect, useState } from "react";
import { fetchGroupPosts, fetchPosts } from "../store/actions/TimeLineActions";

import {
  Navigate,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import CreatePost from "./Modals/CreatePost";
import EditPost from "./Modals/EditPost";
import Reply from "./Modals/Reply";

export default function TimeLine() {
  // Hooks
  const dispatch: Dispatch<any> = useDispatch();
  const navigate = useNavigate();
  const { isAuthenticated } = useAuth0();
  const location = useLocation();
  const { groupId } = useParams();

  // State
  const { post: posts, loading } = useSelector((state: RootState) => state.postData);
  const { user: account } = useSelector((state: RootState) => state.userData);
  const [open, setOpen] = useState(false);
  const [editStatus, setEditStatus] = useState(false);
  const [responseId, setResponseId] = useState<number>();
  const [message, setMessage] = useState("");
  const [replyStatus, setReplyStatus] = useState(false);
  const [parentId, setParentId] = useState<number>();

  /** 
   * Checks if user is authenticated
   * 
   */
  useEffect(() => {
    if (isAuthenticated) {
      const splitPath = location.pathname.split("/");
      if (splitPath.length > 2) {
        if (splitPath[2] === "Group") {
          if (groupId !== undefined) {
            dispatch(fetchGroupPosts(parseInt(groupId)));
          }
        }
      } else {
        dispatch(fetchPosts());
      }
    }
  }, []);

  /**
   * Functions to handle open and close of modal.
   * @returns Boolean if modal is open or not
   */
  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);
  const handleReplyCloseModal = () => setReplyStatus(false);
  const handleCloseEdit = () => setEditStatus(false);
  const handleReplyOpenModal = (parentId: number) => {
    setReplyStatus(true);
    setParentId(parentId);
  }
  const handleOpenEdit = (postId: number) => {
    setEditStatus(true);
    setResponseId(postId);
  }

  return (
    <>
      {!isAuthenticated ? (
        <Navigate to="/" />
      ) : (
        <Body>

          {loading ? (
            <p>Loading ...</p>
          ) : (
            <div className="timeline-head" style={{ textAlign: "right", marginBottom: "15px" }}>
              <Button className="btn bg-secondary btn-outline-light" onClick={() => handleOpenModal()}>
                Create Post
              </Button>
            </div>
          )}
          {posts.length > 0 ? (
            <div>
              {posts.map((post: any) => (
                <Card className="card" key={post.postId} style={{ height: 'fit-content' }}>
                  <Card.Body className="card-body">
                    <div>
                      <label
                        style={{ fontWeight: "bold", cursor: 'pointer' }} //Become a pointer when cursor hover over the posters name
                        onClick={() => navigate(`/UserProfile/${post.senderId}`)}>
                        <h5>
                          {post.posterName}
                        </h5>
                      </label>
                      <div style={{float: "right"}}>
                        {post.groupName ? <p>Group: {post.groupName}</p> : ""}
                        {post.topicName ? <p>Topic: {post.topicName}</p> : ""}
                      </div>
                      <br />
                    </div>
                    <div onClick={() => navigate(`/Thread/${post.postId}`)} style={{ cursor: "pointer" }} >
                      <p>
                        {post.postBody}
                      </p>
                    </div>
                    <br />

                  </Card.Body>
                  <div style={{ textAlign: "right" }}>
                    {post.senderId === account.userId ? (
                      <Button className="btn bg-secondary btn-outline-light" onClick={() => handleOpenEdit(post.postId)}> Edit Post</Button>) : ("")}
                    <Button className="btn bg-secondary btn-outline-light" onClick={() => handleReplyOpenModal(post.postId)}>Post a response</Button>
                    <div style={{ float: "left" }}>

                      {post.timeStamp.split('T').join(' ').split('.')[0]}{/* Makes the timestamp look good */}

                    </div>
                  </div>
                  {/* <ResponsePost></ResponsePost> */}


                </Card>
              ))}
            </div>
          ) : ("")}
        </Body>
      )}
      <Modal
        title="Create Post"
        error={message}
        show={open}
        onHide={handleCloseModal}
      >
        <CreatePost title="Create post" userId={""} handleCloseModal={handleCloseModal} fromProfilePage={false}></CreatePost>
      </Modal>
      <Modal
        title="Edit Post"
        error={message}
        show={editStatus}
        onHide={handleCloseEdit}
      >
        <EditPost postId={responseId} handleCloseModal={handleCloseEdit}></EditPost>
      </Modal>
      <Modal
        title="Reply Post"
        error={message}
        show={replyStatus}
        onHide={handleReplyCloseModal}
      >
        <Reply parentPostId={parentId} handleCloseModal={handleReplyCloseModal}></Reply>
      </Modal>
    </>
  );
}

// let md = '_this_ is **easy** to `use`.';
// let html = snarkdown(md);
// console.log(html);
