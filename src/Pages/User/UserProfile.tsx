import Body from "../../Components/Body/Index";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import { Dispatch, useEffect, useState } from "react";
import { fetchUser } from "../../store/actions/UserActions";
import "./UserProfile.css";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { Container, Row, Col, Button, Modal } from "react-bootstrap";
import { api } from "../../Utils/api";
import { useAuth0 } from "@auth0/auth0-react";
import CreatePost from "../Modals/CreatePost";
import UserSettings from "../Modals/UserSettings";

export default function UserProfile() {
  // Hooks
  const { isAuthenticated } = useAuth0();
  const dispatch: Dispatch<any> = useDispatch();
  const navigate = useNavigate();
  const { userId } = useParams();

  // State
  const { user: account } = useSelector((state: RootState) => state.userData);
  const [open, setOpen] = useState(false);
  const [openSettings, setOpenSettings] = useState(false);
  const [message, setMessage] = useState("");
  const [otherUser, setOtherUser] = useState({
    userId: "",
    username: "",
    firstName: "",
    lastName: "",
    picture: "",
    emailAdress: "",
    status: "",
    bio: "",
    funFact: "",
  });

  /**
   * Checks if current user's id is equal to userid from db. If true navigates to user's profile page.
   * If false, navigates to that user's profile page.
   */
  useEffect(() => {
    if (userId === account.userId)
      navigate("/UserProfile");
    if (userId !== undefined) {
      api
        .getUserByID(userId)
        .then((data) => setOtherUser(data))
        .catch(() => navigate("/notfound", { state: account }));
    }
    dispatch(fetchUser());
  }, []);

  /**
   * Functions to handle open and close of modal.
   * @returns Boolean if modal is open or not
   */
  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);

  const handleOpenSettingsModal = () => setOpenSettings(true);
  const handleCloseSettingsModal = () => setOpenSettings(false);

  return (
    <>
      {!isAuthenticated ? (
        <Navigate to="/" />
      ) : (
        <Body>
          {userId === undefined ? (
            <div className="header">
              <a
                className="btn bg-primary btn-outline-light"
                onClick={handleOpenSettingsModal}
              >
                Settings
              </a>
            </div>
          ) : (
            ""
          )}
          <Container style={{ marginBottom: "25px" }}>
            <Row>
              <Col lg={4}>
                <img
                  src={
                    userId === undefined ? account.picture : otherUser.picture
                  }
                  alt="profile picture"
                  className="user-picture"
                />
              </Col>
              <Col className="col-lg-4">
                <b>Username</b>
                <p>
                  {userId === undefined ? account.username : otherUser.username}
                </p>
                <b>Bio</b>
                <p>{userId === undefined ? account.bio : otherUser.bio}</p>
              </Col>
              <Col className="col-lg-3">
                <b>Status</b>
                <p>
                  {userId === undefined ? account.status : otherUser.status}
                </p>
                <b>Fun fact</b>
                <p>
                  {userId === undefined ? account.funFact : otherUser.funFact}
                </p>
              </Col>
            </Row>
          </Container>
          <div style={{float: "right"}}>
            {userId === undefined ? (
              ""
            ) : (
              <Button onClick={() => navigate(`/Groups/${userId}`)}>
                invite to group
              </Button>
            )}
            {userId !== undefined ? (
              <Button onClick={() => handleOpenModal()} style={{marginLeft: "10px"}}>
                send direct message
              </Button>
            ) : (
              ""
            )}
          </div>

        </Body>
      )}
      <Modal
        title="Create Post"
        alt="direct Message"
        error={message}
        show={open}
        onHide={handleCloseModal}
      >
        <CreatePost title="Direct Message" userId={userId!} handleCloseModal={handleCloseModal} fromProfilePage={true}></CreatePost>
      </Modal>
      <Modal title="User Settings" error={message} show={openSettings} onHide={handleCloseSettingsModal}>
        <UserSettings handleCloseModal={handleCloseSettingsModal}></UserSettings>
      </Modal>
    </>
  );
}
