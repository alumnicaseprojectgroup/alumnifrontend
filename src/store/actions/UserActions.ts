
import { User, UserEdit } from "../../Models/User"

export const FETCH_USER = '[user] FETCH'
export const SET_USER = '[user] SET'
export const SET_USER_ERROR = '[user] SET-ERROR'
export const PATCH_USER = '[user] PATCH'

export function fetchUser(){
    return {
        type: FETCH_USER
    }
}


export function setUser(payload: User){
    return {
        type: SET_USER,
        payload
    }
}

export function setUserError(payload: string){
    return {
        type: SET_USER_ERROR,
        payload
    }
}

export function patchUser(payload: UserEdit, payload2: number){
    return {
        type: PATCH_USER,
        payload,
        payload2
    }
}