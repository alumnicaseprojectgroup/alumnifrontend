import { Posts, PostsCreate, PostsCreateAwnser, PostsEdit } from "../../Models/Posts"

export const FETCH_POSTS = '[post] FETCH'
export const SET_POSTS = '[post] SET'
export const SET_POSTS_ERROR = '[post] SET-ERROR'
export const POST_POSTS = '[post] POST'
export const PUT_POSTS = '[post] PUT'
export const FETCH_GROUP_POSTS = '[post] FETCH_GROUP'
export const POST_RESPONSE = '[post] POST-RESPONSE'

export function fetchPosts(){
    return {
        type: FETCH_POSTS
    }
}

export function fetchGroupPosts(payload: number){
    return {
        type: FETCH_GROUP_POSTS,
        payload
    }
}

export function setPosts(payload: Posts[]){
    return {
        type: SET_POSTS,
        payload: payload
    }
}

export function setPostsError(payload: string){
    return {
        type: SET_POSTS_ERROR,
        payload
    }
}

export function postPost(payload: PostsCreate){
    return {
        type: POST_POSTS,
        payload
    }
}

export function patchPost(payload: PostsEdit, payload2: number){
    return {
        type: PUT_POSTS,
        payload,
        payload2
    }
}

export function AwnserPost(payload: PostsCreateAwnser, payload2: number){
    return{
        type: POST_RESPONSE,
        payload,
        payload2
    }
}