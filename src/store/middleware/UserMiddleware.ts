import { api } from "../../Utils/api";
import {
  FETCH_USER,
  PATCH_USER,
  setUser,
  setUserError,
} from "../actions/UserActions";

export function UserMiddleware({ dispatch }: any) {
  return function (next: any) {
    return function (action: any) {
      next(action);

      if (action.type === FETCH_USER) {
        api
          .getUser()
          .then((user) => dispatch(setUser(user)))
          .catch((error: Error) => dispatch(setUserError(error.message)));
      }

      if (action.type === PATCH_USER) {
        api
          .patchUser(action.payload, action.payload2)
          .then(() => dispatch(setUser(action.payload)))
          .catch((error: Error) => dispatch(setUserError(error.message)));
      }
    };
  };
}
