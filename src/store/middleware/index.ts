import {applyMiddleware} from "redux";
import { UserMiddleware } from "./UserMiddleware";
import { PostMiddleware } from "./TimeLineMiddleware";

export default applyMiddleware(UserMiddleware,PostMiddleware);