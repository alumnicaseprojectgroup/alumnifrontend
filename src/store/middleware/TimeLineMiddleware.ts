import { api } from "../../Utils/api";
import { FETCH_POSTS, PUT_POSTS, POST_POSTS, setPosts, setPostsError, FETCH_GROUP_POSTS, POST_RESPONSE, fetchPosts } from "../actions/TimeLineActions";

export function PostMiddleware({ dispatch }: any) {
  return function (next: any) {
    return function (action: any) {
      next(action);

      if (action.type === FETCH_POSTS) {
        api.getPosts()
          .then(post => dispatch(setPosts(post)))
          .catch((error: Error) => dispatch(setPostsError(error.message)));
      }
      if (action.type === POST_POSTS) {
        api
          .postPost(action.payload)
          .then(() => dispatch(fetchPosts()))
          .catch((error: Error) => dispatch(setPostsError(error.message)));
      }
      if (action.type === PUT_POSTS) {
        api
          .patchPost(action.payload, action.payload2)
          .then(() => dispatch(fetchPosts()))
          .catch((error: Error) => dispatch(setPostsError(error.message)));
      }
      if(action.type === FETCH_GROUP_POSTS){
        api
        .getPostsByGroupId(action.payload)
        .then((post)=>dispatch(setPosts(post)))
        .catch((error: Error)=> dispatch(setPostsError(error.message)));
      }
      if(action.type === POST_RESPONSE){
        api
          .AwnserPost(action.payload)
          .then(() => dispatch(fetchPosts()))
          .catch((error: Error) => dispatch(setPostsError(error.message)));
      }
    };
  };
}