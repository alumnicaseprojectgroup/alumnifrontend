import { combineReducers } from "redux";
import { UserReducer } from "./UserReducer";
import { PostsReducer } from "./TimeLineReducer";

const appReducer = combineReducers({
    userData: UserReducer,
    postData: PostsReducer,
})

export type RootState = ReturnType<typeof appReducer>
export default appReducer;