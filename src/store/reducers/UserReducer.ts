import {
  FETCH_USER,
  SET_USER,
  SET_USER_ERROR,
  PATCH_USER,
} from "../actions/UserActions";

const initialState = {
  error: "",
  loading: false,
  user: {},
};

export function UserReducer(state = initialState, action: any) {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        loading: true,
      };
    case SET_USER:
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case SET_USER_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case PATCH_USER:
      return {
        ...state,
        loading: true,
      };

    default:
      return state;
  }
}
