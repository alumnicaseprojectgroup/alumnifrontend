import { Posts } from "../../Models/Posts";
import { FETCH_POSTS, SET_POSTS, SET_POSTS_ERROR, POST_POSTS, PUT_POSTS, POST_RESPONSE } from "../actions/TimeLineActions";

interface postsState {
  error: string;
  loading: boolean;
  post: Posts[];
}

const initialState: postsState = {
  error: "",
  loading: false,
  post: [],
};

export function PostsReducer(state = initialState, action: any) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        loading: true,
      };
    case SET_POSTS:
      return {
        ...state,
        loading: false,
        post: action.payload,
      };
    case SET_POSTS_ERROR:
      return {
        ...state,
        error: action.payload,
      };
      case POST_POSTS:
      return {
        ...state,
        loading: true,
      };
      case PUT_POSTS:
      return {
        ...state,
        loading: true,
      };
      case POST_RESPONSE:
        return{
          ...state,
          loading: true,
        };
    default:
      return state;
  }
}