import axios from "axios";
import { Posts } from "../Models/Posts";
import { conversationUser, User } from "../Models/User";
import { Topic, TopicUser } from "../Models/Topic";
import { Group, GroupUser } from "../Models/Group";

//const BASE_URL = "https://localhost:44335/api/v1";
const BASE_URL = "https://alumniwebapi.azurewebsites.net/api/v1";

/*
* Sets user bearer token.
*/
export const api = {
  token: "",

  getConfig: function () {
    if (!this.token) {
      return {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: "*",
        },
      };
    }
    return {
      headers: {
        Authorization: `Bearer ${this.token}`,
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    };
  },

  setToken: async function (getAccessTokenSilently: any) {
    if (this.token === "") {
      try {
        this.token = await getAccessTokenSilently();
      } catch {
        this.token = "";
      }
    }
  },

  /********************/
  /** USER API CALLS **/
  /********************/

  // Gets user from db
  getUser: async function (): Promise<User> {
    const res = await axios.get(`${BASE_URL}/Users`, this.getConfig());
    return res.data;
  },

  // Patches user in db.
  patchUser: async function (
    updatedUser: User,
    id: number
  ): Promise<undefined> {
    await axios.patch(
      `${BASE_URL}/Users/userId?userId=${id}`,
      updatedUser,
      this.getConfig()
    );
    return;
  },
  getUserByID: async function (userId: string): Promise<User> {
    const res = await axios.get(
      `${BASE_URL}/Users/userId?userId=${userId}`,
      this.getConfig()
    );
    return res.data;
  },

  /********************/
  /** POST API CALLS **/
  /********************/

  // Gets posts from db.
  getPosts: async function (): Promise<Posts[]> {
    const rest = await axios.get(`${BASE_URL}/Posts`, this.getConfig());
    return rest.data;
  },

  // Creates post and adds to db.
  postPost: async function (createPost: Posts): Promise<Posts> {
    const rest = await axios.post(
      `${BASE_URL}/Posts`,
      createPost,
      this.getConfig()
    );
    return rest.data;
  },

  // Updates values for a post in db.
  patchPost: async function (updatedPost: Posts, id: number): Promise<Posts> {
    const rest = await axios.put(
      `${BASE_URL}/Posts/${id}`,
      updatedPost,
      this.getConfig()
    );
    return rest.data;
  },

  // Creates a new post, creates connection with parent post and adds to db. 
  AwnserPost: async function (AwnserPost: Posts): Promise<Posts> {
    const rest = await axios.post(
      `${BASE_URL}/Posts`,
      AwnserPost,
      this.getConfig()
    );
    return rest.data;
  },

  // Gets a specific post by id.
  GetPostById: async function (postId: number): Promise<Posts> {
    const res = await axios.get(
      `${BASE_URL}/Posts/${postId}`,
      this.getConfig()
    );
    return res.data;
  },
  
  // Gets all posts that have the same post as parent. 
  getThreadById: async function (postId: number): Promise<Posts[]> {
    const res = await axios.get(
      `${BASE_URL}/Posts/thread/${postId}`,
      this.getConfig()
    );
    return res.data;
  },
  /*********************/
  /** GROUP API CALLS **/
  /*********************/

  // Gets all groups from db.
  getGroups: async function (): Promise<Group[]> {
    const res = await axios.get(`${BASE_URL}/Groups`, this.getConfig());
    return res.data;
  },

  // Creates a new group and adds to db.
  postGroup: async function (group: object): Promise<boolean> {
    const res = await axios.post(`${BASE_URL}/Groups`, group, this.getConfig());
    if (res.status === 201) {
      return true;
    }
    return false;
  },

  // Gets posts that are connected to a specific group.
  getPostsByGroupId: async function (groupId: number): Promise<Posts[]> {
    const res = await axios.get(
      `${BASE_URL}/Posts/group/${groupId}`,
      this.getConfig()
    );
    return res.data;
  },

  // Adds relation between a group and a user.
  joinGroup: async function (groupId: number): Promise<boolean> {
    const user: GroupUser = { userId: null };
    const res = await axios.post(
      `${BASE_URL}/Groups/${groupId}/join`,
      user,
      this.getConfig()
    );
    if (res.status === 201) {
      return true;
    }
    return false;
  },

  // Gets specific group by Id.
  getGroupById: async function (groupId: number): Promise<Group> {
    const res = await axios.get(
      `${BASE_URL}/Groups/${groupId}`,
      this.getConfig()
    );
    return res.data;
  },

  // Adds relation between a group and user.
  addToGroup: async function (
    groupId: number,
    userId: GroupUser
  ): Promise<boolean> {
    const res = await axios.post(
      `${BASE_URL}/Groups/${groupId}/join`,
      userId,
      this.getConfig()
    );
    if (res.status === 201) {
      return true;
    }
    return false;
  },

  /*********************/
  /** TOPIC API CALLS **/
  /*********************/
  
  // Gets all topics from db.
  getTopics: async function (): Promise<Topic[]> {
    const res = await axios.get(`${BASE_URL}/Topics`, this.getConfig());
    return res.data;
  },

  // Creates a new topic and adds to db.
  postTopic: async function (topic: object): Promise<boolean> {
    const res = await axios.post(`${BASE_URL}/Topics`, topic, this.getConfig());
    if (res.status === 201) {
      return true;
    }
    return false;
  },

  // Gets specific topic by id. 
  getTopicById: async function (topicId: number): Promise<Topic> {
    const res = await axios.get(
      `${BASE_URL}/Topics/${topicId}`,
      this.getConfig()
    );
    return res.data;
  },

  // Creates relation between a topic and a user.
  joinTopic: async function (topicId: number): Promise<boolean> {
    const user: TopicUser = { userId: null };
    const res = await axios.post(
      `${BASE_URL}/Topics/${topicId}/join`,
      user,
      this.getConfig()
    );
    if (res.status === 201) {
      return true;
    }
    return false;
  },

  /*****************************/
  /** DIRECTMESSAGE API CALLS **/
  /*****************************/

  // Gets all directmessage connections between two userid's.
  getDirectMessages: async function (): Promise<conversationUser[]> {
    const res = await axios.get(`${BASE_URL}/Posts/user`, this.getConfig());
    return res.data;
  },

  // Gets all messages between two userid's.
  GetConversationById: async function (userId: string): Promise<Posts[]> {
    const res = await axios.get(
      `${BASE_URL}/Posts/user/${userId}`,
      this.getConfig()
    );
    return res.data;
  },
};
