# Alumni Network Case Project Web App

This is the Web App for the Alumni Network case project for Noroff Accelerate winter 2022. The app is build with React with typescript.

## Description

The app contains six main views.

### Login
The first page a user will see is the login page which will prompt the user to login. The login button will redirect the user to Auth0 which handles the authentication and authorisation of our app.

### Home/Timeline 
After successfully loging in the user will land on the home page which contains all posts the user has subscribed to see. The user can create a new post here, aswell as edit past posts and reply to other posts. Clicking on a post will redirect the user to thread view that shows the post and its replies. 

### Direct Messages
The direct messages view will show all personal conversations the user is active in.

### Groups
The groups page shows the groups the user has joined and public groups the user can join. Here the user can also create a new group.

### Topics
The topics page shows all existing topics that can be joined. The user can also create a new topic here.

### Profile page
The profile page shows the users profile. If the user checks their own profile page the user has the ability to edit the user settings. If the user checks another user's profile page the user can invite them to a group or send them a direct message.

## Getting Started

### Dependencies

* Any modern well used web browser (chrome, firefox, edge etc.)
* Node.js
* Npm

### Installing
* clone repository / download
* Open terminal and navigate to project folder and enter 
```
npm install
```

### Executing program

* In a terminal navigate to project folder and enter
```
npm start
```
* Navigate in a browser to http://localhost:3000/ (or other port declared by terminal if port is in use)
* Or alternatively go to this link to use the program: https://alumniwebapp.azurewebsites.net/


## Authors

Jessica Lindqvist [@Jessica.Lindqvist]

Lukas Mårtensson [@Hela042]

Christian Andersz [@christianandersz]